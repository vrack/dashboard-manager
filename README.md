# Dashboard Manager

Интерфейс для работы с VRack

На данный момент интерфейс находится внешне в плачевном но рабочем состоянии, что может:

- Добавление нескольких подключений и хранение информации о них в localstorage
- Просмотр дашбордов
    - Получение списка дашбордов и ослеживания их статуса в онлайн режиме
    - Управление дашбордами (запуск/перезапуск/остановка)
    - Получение ошибок дашборда
    - Работа с устройствами
        - Отправка Action
        - Мониторинг исходящих портов
        - Мониторинг событий
        - Просмотр Shares данных
    - Графическая схема дашборда
        - Сохранение нового расположения
- Управления ключами сервера
- Автоматическая документация (начиная с версии VRack 1.0.0)


## Установка

```
git clone https://gitlab.com/vrack/dashboard-manager.git
```

```
npm install
```

### Для develop сервера

```
npm run serve
```

После чего можно уже зайти на соответсвующий адрес указанный в консоли


### Для локальной статики

```
npm run build
```