module.exports = {
  publicPath: '',
  pluginOptions: {
    moment: {
      locales: [
        'ru'
      ]
    },
    electronBuilder: {
      nodeIntegration: true
    }
  }
}
