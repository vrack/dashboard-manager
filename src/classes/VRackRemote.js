const EventEmitter = require('events')
const CryptoJS = require('crypto-js')

module.exports = class extends EventEmitter {
  constructor (address, key) {
    super()
    this.address = 'ws://localhost:4044/'
    this._pkgIndex = 1000
    this.key = ''
    this.private = ''
    this.level = 1000
    this.connected = false
    this.connection = false
    this.disconnected = false
    this.channels = new Map()
    this._ws = false
    this._queue = new Map()
    this._queueTimeout = new Map()
    this.setAddress(address)
    this.setKey(key)
  }

  async apiKeyAuth (key) {
    var result = await this.commandPromise('apiKeyAuth', { data: { key: key } })
    if (result.resultData.cipher) {
      result = await this.commandPromise('apiPrivateAuth', {
        data: { verify: this.cipherData(result.resultData.verify).toString() }
      })
    }
    this.level = result.resultData.level
    this.cipher = result.resultData.cipher
    this.authorized = true
    return result
  }

  async apiKeyAdd (name, description, level, cipher) {
    return await this.commandPromise('apiKeyAdd', { data: { 
      name,
      description,
      level,
      cipher
    } })
  }

  async apiKeyUpdate (name, description, key) {
    return await this.commandPromise('apiKeyUpdate', { data: { 
      name,
      description,
      key
    }})
  }

  async apiKeyDelete (key) {
    return await this.commandPromise('apiKeyDelete', { data: {key} })
  }

  async apiKeyList () {
    return await this.commandPromise('apiKeyList', {})
  }

  async channelJoin (channel, cb) {
    const result = this.commandPromise('channelJoin', { data: { channel: channel } })
    this.channels.set(channel, cb)
    return result
  }

  async channelLeave (channel) {
    const result = await this.commandPromise('channelLeave', { data: { channel: channel } })
    this.channels.delete(channel)
    return result
  }

  async channelsLeave (channels) {
    const result = await this.commandPromise('channelsLeave', { data: { channels: channels } })
    for (var channel of channels) this.channels.delete(channel)
    return result
  }

  async channelLeaveAll () {
    const result = await this.commandPromise('channelLeaveAll', { data: {} })
    this.channels = new Map()
    return result
  }

  dashboardRun (dashboard) {
    return this.commandPromise('dashboardRun', { data: { dashboard } })
  }

  dashboardCheck (dashboard) {
    return this.commandPromise('dashboardCheck', { data: { dashboard } })
  }


  dashboard (dashboard) {
    return this.commandPromise('dashboard', { data: { dashboard } })
  }

  dashboardStop (dashboard) {
    return this.commandPromise('dashboardStop', { data: { dashboard } })
  }

  dashboardErrors (dashboard) {
    return this.commandPromise('dashboardErrors', { data: { dashboard } })
  }

  dashboardErrorsClear (dashboard) {
    return this.commandPromise('dashboardErrorsClear', { data: { dashboard } })
  }

  dashboardListUpdate () {
    return this.commandPromise('dashboardListUpdate', {})
  }

  deviceListUpdate () {
    return this.commandPromise('deviceListUpdate', {})
  }


  dashboardList () {
    return this.commandPromise('dashboardList', {})
  }

  dashboardMeta (dashboard) {
    return this.commandPromise('dashboardMeta', { data: { dashboard } })
  }

  dashboardStructure (dashboard) {
    return this.commandPromise('dashboardStructure', { data: { dashboard } })
  }

  dashboardStructureUpdate (dashboard, structure) {
    return this.commandPromise('dashboardStructureUpdate', { data: { dashboard, structure } })
  }

  dashboardDeviceAction (dashboard, device, action, push) {
    return this.commandPromise('dashboardDeviceAction', {
      data: { action, device, dashboard, push }
    })
  }

  dashboardDeviceOutputListen (dashboard, device, port, timeout) {
    return this.commandPromise('dashboardDeviceOutputListen', {
      data: { dashboard, device, port, timeout }
    })
  }


  dashboardShares (dashboard) {
    return this.commandPromise('dashboardShares', { data: { dashboard } })
  }

  setAddress (address) { this.address = address }
  setKey (key) { this.key = key }

  connect () {
    if (this.connected === true) this.disconnect()
    this.disconnected = false
    this.connection = true
    this._ws = new WebSocket(this.address)

    this._ws.onopen = () => {
      this.connected = true
      this.connection = false
      this.emit('open')
    }

    this._ws.onclose = () => {
      this.connected = false
      this.connection = false
      this.emit('close')
      this._ws = null
    }

    this._ws.onmessage = (evt) => {
      var data = evt.data
      if (this.cipher) data = this.decipherData(data)
      const remoteData = JSON.parse(data)
      if (remoteData._pkgIndex) {
        if (this._queue.has(remoteData._pkgIndex)) {
          clearTimeout(this._queueTimeout.get(remoteData._pkgIndex))
          // console.log(remoteData)
          var func = this._queue.get(remoteData._pkgIndex)
          if (remoteData.result === 'error') {
            // console.log(remoteData.resultData)
            func.reject(this.errorify(remoteData.resultData))
          } else {
            func.resolve(remoteData)
          }
          this._queue.delete(remoteData._pkgIndex)
          this._queueTimeout.delete(remoteData._pkgIndex)
        }
      } else if (remoteData.command === 'broadcast') {
        if (this.channels.has(remoteData.target)) {
          this.channels.get(remoteData.target)(remoteData)
        }
      }
    }
  }

  cipherData (data) {
    return CryptoJS.AES.encrypt(data, CryptoJS.enc.Utf8.parse(this.private), {
      iv: CryptoJS.enc.Utf8.parse(this.key),
      mode: CryptoJS.mode.CBC
    })
  }

  decipherData (data) {
    const res = CryptoJS.AES.decrypt(data, CryptoJS.enc.Utf8.parse(this.private), {
      iv: CryptoJS.enc.Utf8.parse(this.key),
      mode: CryptoJS.mode.CBC
    })
    return res.toString(CryptoJS.enc.Utf8)
  }

  disconnect () {
    this._disconnect = true
    this.authorized = false
    if (this._ws) this._ws.close()
  }

  errorify(error){
    const result = new Error()
    for (const key of Object.getOwnPropertyNames(error)) result[key] = error[key]
    return result
  }

  commandPromise (command, params) {
    return new Promise((resolve, reject) => {
      this.command(command, params, resolve, reject)
    })
  }

  command (command, params, resolve, reject) {
    if (params === undefined) params = {}
    params.command = command
    params._pkgIndex = this._pkgIndex
    this._pkgIndex++
    this.addToQueue(params, resolve, reject)
  }

  addToQueue (params, resolve, reject) {
    this._queue.set(params._pkgIndex, { resolve: resolve, reject: reject })
    this._queueTimeout.set(params._pkgIndex, setTimeout(() => {
      reject('Timeout')
      this._queue.delete(params._pkgIndex)
      this._queueTimeout.delete(params._pkgIndex)
    }, 5000))

    if (this.connected) {
      var data = JSON.stringify(params)
      if (this.cipher) data = this.cipherData(data)
      this._ws.send(data)
    } else {
      reject('WebSocket is close')
    }
  }
}
