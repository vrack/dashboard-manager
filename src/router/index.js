import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Manager from '../views/Manager.vue'
import Dashboard from '../views/watch/Dashboard.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    name: 'manager',
    path: '/manager/:id',
    component: Manager,
    children: [
      {
        name: 'devicelist',
        path: 'devicelist',
        component: () => import('../views/DeviceList.vue')
      },
      {
        name: 'keys',
        path: 'keys',
        component: () => import('../views/watch/Keys.vue')
      },
      {
        name: 'dashboard',
        path: ':dashboard',
        component: Dashboard,
        children: [
          {
            name: 'control',
            path: 'control',
            component: () => import('../views/dashboard/Control.vue')
          },
          {
            name: 'devices',
            path: 'devices',
            component: () => import('../views/dashboard/Devices.vue')
          },
          {
            name: 'scheme',
            path: 'scheme',
            component: () => import('../views/dashboard/Scheme.vue')
          }
        ]
      }
    ]
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
